//
//  Request.swift
//  SixtTest
//
//  Created by Константин Овчаренко on 5/7/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation

class Request {

    typealias complitionHablerImages = (_ images : [CarsModel]) -> ()

    func request( complition : @escaping complitionHablerImages ){
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://www.codetalk.de/cars.json")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                            print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("this is httResponse, \(httpResponse as Any)")
            }
            guard let data = data else {return}
            print(data)
            do{
                
                let  dataInfo = try JSONDecoder().decode([CarsModel].self, from: data)
                print(dataInfo as Any)
                complition(dataInfo)
                
            } catch{
                print("Somsing with Decoder",error)
            }
        })
        dataTask.resume()
    }
}
