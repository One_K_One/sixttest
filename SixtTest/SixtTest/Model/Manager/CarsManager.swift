//
//  RequestManager.swift
//  SixtTest
//
//  Created by Константин Овчаренко on 5/21/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation


class CarsManager {
    
    static let shared = CarsManager()
    let request = ApiManager.shared
    
    private(set) var cars: [CarsModel] = []
    
    private init(){
        
        request.getCars(complition: { (cars :[CarsModel]) in
            self.cars.append(contentsOf: cars)
            
            // Правильно сделать отдельный менеджер, но т.к. тестовое, решил не заморачиваться 
            let notificationName = NSNotification.Name.init("CarsManager_GetData")
            NotificationCenter.default.post(name: notificationName, object: nil)
        })
    }
}
