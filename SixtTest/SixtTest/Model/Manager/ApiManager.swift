//
//  ApiManager.swift
//  SixtTest
//
//  Created by Константин Овчаренко on 5/8/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation

class ApiManager  {
    
    static let shared = ApiManager()
    
    func getCars( complition : @escaping ([CarsModel]) -> ()) {
        
        let request = Request()
        request.request() { (carModels :[CarsModel]) in
            OperationQueue.main.addOperation {
                complition(carModels)
            }
        }
    }
}
