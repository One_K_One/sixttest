//
//  CarsModel.swift
//  SixtTest
//
//  Created by Константин Овчаренко on 5/7/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation

class CarsModel : Codable {
    let name             : String
    let modelName        : String
    let make             : String
    let color            : String
    let fuelType         : String
    let transmission     : String
    let innerCleanliness : String
    let carImageUrl      : String
    let flueLavel        : Double
    let latitude         : Double
    let longitude        : Double
    
    
    enum CodingKeys: String, CodingKey {
        case name             = "name"
        case modelName        = "modelName"
        case make             = "make"
        case color            = "color"
        case fuelType         = "fuelType"
        case transmission     = "transmission"
        case innerCleanliness = "innerCleanliness"
        case carImageUrl      = "carImageUrl"
        case flueLavel        = "fuelLevel"
        case latitude         = "latitude"
        case longitude        = "longitude"
        
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        name             = try container.decode(String.self, forKey: .name)
        modelName        = try container.decode(String.self, forKey: .modelName)
        make             = try container.decode(String.self, forKey: .make)
        color            = try container.decode(String.self, forKey: .color)
        fuelType         = try container.decode(String.self, forKey: .fuelType)
        transmission     = try container.decode(String.self, forKey: .transmission)
        innerCleanliness = try container.decode(String.self, forKey: .innerCleanliness)
        carImageUrl      = try container.decode(String.self, forKey: .carImageUrl)
        flueLavel        = try container.decode(Double.self, forKey: .flueLavel)
        latitude         = try container.decode(Double.self, forKey: .latitude)
        longitude        = try container.decode(Double.self, forKey: .longitude)
    }
}
