//
//  MapViewController.swift
//  SixtTest
//
//  Created by Константин Овчаренко on 5/7/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapViewController : UIViewController,  MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var bigMap: MKMapView!
    
    let carsManager = CarsManager.shared

    let locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.bigMap.showsUserLocation = true
       
            for carsModel in carsManager.cars{
                
                let annotation = MKPointAnnotation()
                let latitude = carsModel.latitude
                let longitude = carsModel.longitude
                
                annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude:longitude)
                bigMap.addAnnotation(annotation)
            }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        
        self.bigMap.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors " + error.localizedDescription)
    }
}

/*
 реализацию по хорошему вынести и сделать по аналогии с tableVC
 */
