//
//  ListTableViewCell.swift
//  SixtTest
//
//  Created by Константин Овчаренко on 5/8/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation
import UIKit


class ListTableViewCell : UITableViewCell {
    
    
    @IBOutlet weak var CarsImage: UIImageView!
    
    @IBOutlet weak var LabelMake: UILabel!
    
    @IBOutlet weak var LabelName: UILabel!
    
    @IBOutlet weak var LabelModelName: UILabel!
    
    @IBOutlet weak var LabelInnerCleanliness: UILabel!
    
    @IBOutlet weak var LabelColor: UILabel!
    
    @IBOutlet weak var LabelFluelType: UILabel!
    
    @IBOutlet weak var LabelTransmision: UILabel!
        
    @IBOutlet weak var LabelFluelLavel: UILabel!
    
    
    override func prepareForReuse() {
        CarsImage.image = nil
    }
    
}
