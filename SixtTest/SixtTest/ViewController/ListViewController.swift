//
//  ViewController.swift
//  SixtTest
//
//  Created by Константин Овчаренко on 5/7/19.
//  Copyright © 2019 Константин Овчаренко. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var TableView: UITableView!

    let carsManager = CarsManager.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateTable),
            name: NSNotification.Name(rawValue: "CarsManager_GetData"),
            object: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return carsManager.cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListTableViewCell
       
        let carsModel = carsManager.cars[indexPath.row]
        Alamofire.request(carsModel.carImageUrl).responseImage { response in
            guard let image = UIImage(data:response.data!) else {
                // Handle error
                return
                }
            let imageData = UIImage.jpegData(image)(compressionQuality: 1.0)
            cell.CarsImage?.image            = UIImage(data : imageData!)
        }
        
            cell.LabelMake?.text             = " Make : \(carsModel.make)"
            cell.LabelName?.text             = " Name : \(carsModel.name)"
            cell.LabelModelName?.text        = " Model name : \(carsModel.modelName)"
            cell.LabelInnerCleanliness?.text = " Inner Clean liness : \(carsModel.innerCleanliness)"
            cell.LabelColor?.text            = " Color  : \(carsModel.color)"
            cell.LabelFluelType?.text        = " Type fuel : \(carsModel.fuelType)"
            cell.LabelTransmision?.text      = " Transmission  : \(carsModel.transmission)"
            cell.LabelFluelLavel?.text       = " Fluel Lavel : \(Int(carsModel.flueLavel * 100))%"
         
        return cell
    }
    
    @objc func updateTable(){
        self.TableView.reloadData()
    }
}

